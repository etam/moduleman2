#!/bin/bash

cd "$(dirname "$0")"

die() {
    echo "$1" >&2
    exit 1
}

# === Settings ===

module_dest_dir="${HOME}/puppet/modules"

dashboard=true
dashboard_user="puppet-dashboard"
dashboard_dir="/usr/share/puppet-dashboard"

[[ -r moduleman.conf ]] && source moduleman.conf


# === Flags ===

source shflags

DEFINE_string module_dest_dir "$module_dest_dir" "Directory, where modues are stored."

DEFINE_boolean dashboard "$dashboard" "If false, class is not unregistered from Dashboard. All other dashboard options are ignored."
DEFINE_string dashboard_user "$dashboard_user" "User, which is used to run Dashboard."
DEFINE_string dashboard_dir "$dashboard_dir" "Directory containing Rakefile with Dashboard's Rake API."

FLAGS_HELP="USAGE: $0 [flags] modulename"


FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# convert variables "FLAGS_name" to "name"
for flag_name in $__flags_longNames; do
    [[ "$flag_name" == "help" ]] && continue
    value_variable_name=FLAGS_$flag_name
    declare $flag_name="${!value_variable_name}"
done

# Positional parameters

[[ $# -eq 1 ]] \
    || die "expected 1 positional argument (see --help for more info)"

name="$1"


# === Checks ===

# http://docs.puppetlabs.com/puppet/3/reference/modules_fundamentals.html#allowed-module-names
[[ "$name" =~ ^[a-z][a-z0-9_]*$ ]] \
    || die "error: invalid modulename \"$name\""

[[ -d "$module_dest_dir/$name" ]] \
    || die "error: there is no module \"$name\" in directory \"$module_dest_dir\""

if [[ $dashboard == $FLAGS_TRUE ]]; then
    id "$dashboard_user" >/dev/null 2>&1 \
        || die "error: dashboard_user \"$dashboard_user\" does not exist"
    
    [[ -f "$dashboard_dir/Rakefile" ]] \
        || die "error: dashboard_dir \"$dashboard_dir\" does not contain Rakefile"
fi


# === Remove Class From Dashboard ===

if [[ $dashboard == $FLAGS_TRUE ]]; then
    echo "info: removing class from Dashboard"
    # rake fails if it cannot access directory where command was run.
    pushd ${TMPDIR-/tmp} >/dev/null
        sudo -u "$dashboard_user" env RAILS_ENV=production rake --silent --rakefile "$dashboard_dir/Rakefile" nodeclass:del name=$name \
            || die "error: removing class from Dashboard failed"
    popd >/dev/null
fi

echo "info: removing module"
rm -fr "$module_dest_dir/$name"

echo "info: operation successfully finished"
