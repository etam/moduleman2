require 'puppet'
require 'puppet/face'

packages = Puppet::Face[:resource, '0'].search("package")

packages.each do |package|
    Facter.add("package " + package.title) do
        setcode { package[:ensure] }
    end
    Facter.add("package " + package.title + " provider") do
        setcode { package[:provider] }
    end
end
