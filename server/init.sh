#!/bin/bash

cd "$(dirname "$0")"

die() {
    echo "$1" >&2
    exit 1
}

# === Settings ===

module_dest_dir="${HOME}/puppet/modules"

dashboard=true
dashboard_user="puppet-dashboard"
dashboard_dir="/usr/share/puppet-dashboard"

[[ -r moduleman.conf ]] && source moduleman.conf


# === Flags ===

source shflags

DEFINE_string module_dest_dir "$module_dest_dir" "Directory, where module will be created"

DEFINE_boolean dashboard "$dashboard" "If false, class is not registered in Dashboard. All other dashboard options are ignored."
DEFINE_string dashboard_user "$dashboard_user" "User, which is used to run Dashboard."
DEFINE_string dashboard_dir "$dashboard_dir" "Directory containing Rakefile with Dashboard's Rake API."


FLAGS_HELP="USAGE: $0 [flags] modulename"

FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# convert variables "FLAGS_name" to "name"
for flag_name in $__flags_longNames; do
    [[ "$flag_name" == "help" ]] && continue
    value_variable_name=FLAGS_$flag_name
    declare $flag_name="${!value_variable_name}"
done

# Positional parameters

[[ $# -eq 1 ]] \
    || die "expected 1 positional argument (see --help for more info)"

name="$1"


# === Checks ===

# modulename
# http://docs.puppetlabs.com/puppet/3/reference/modules_fundamentals.html#allowed-module-names
[[ "$name" =~ ^[a-z][a-z0-9_]*$ && "$name" != "main" && "$name" != "settings" ]] \
    || die "error: invalid modulename \"$name\""

[[ -d "$module_dest_dir" ]] \
    || die "error: module_dest_dir \"$module_dest_dir\" is not a directory"

[[ ! -d "$module_dest_dir/$name" ]] \
    || die "error: module \"$name\" already exists"

if [[ $dashboard == $FLAGS_TRUE ]]; then
    id "$dashboard_user" >/dev/null 2>&1 \
        || die "error: dashboard_user \"$dashboard_user\" does not exist"
    
    [[ -f "$dashboard_dir/Rakefile" ]] \
        || die "error: dashboard_dir \"$dashboard_dir\" does not contain Rakefile"
fi


# === Main ===

mkdir "$module_dest_dir/$name"
pushd "$module_dest_dir/$name" >/dev/null
    echo "info: creating module"
    
    git init --quiet
    git config user.name "init script"
    git config user.email "$USER@localhost"
    
    mkdir manifests
    echo -e "class $name {\n}" > manifests/init.pp
    
    git add *
    git commit --quiet -m "init"
    
    git config receive.denyCurrentBranch ignore
    rm -r .git/hooks
    cp -r "$(dirs -l +1)/hooks" .git/
popd >/dev/null


# === Add Class To Dashboard ===

if [[ $dashboard == $FLAGS_TRUE ]]; then
    echo "info: adding class to Dashboard"
    # rake fails if it cannot access directory where command was run.
    pushd ${TMPDIR-/tmp} >/dev/null
        sudo -u "$dashboard_user" env RAILS_ENV=production rake --silent --rakefile "$dashboard_dir/Rakefile" nodeclass:add name=$name \
            || die "error: adding class to Dashboad failed"
    popd >/dev/null
fi

echo "info: operation successfully finished"
