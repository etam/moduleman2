#!/bin/bash

cd "$(dirname "$0")"

die() {
    echo "$1" >&2
    exit 1
}

# === Settings ===

module_dest_dir="${HOME}/puppet/modules"

[[ -r moduleman.conf ]] && source moduleman.conf


# === Flags ===

source shflags

DEFINE_string module_dest_dir "$module_dest_dir" "Directory, containing modules"

FLAGS_HELP="USAGE: $0 [flags] action modulename certname"

FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# convert variables "FLAGS_name" to "name"
for flag_name in $__flags_longNames; do
    [[ "$flag_name" == "help" ]] && continue
    value_variable_name=FLAGS_$flag_name
    declare $flag_name="${!value_variable_name}"
done

# Positional parameters

[[ $# -eq 3 ]] \
    || die "expected 3 positional argument (see --help for more info)"

action="$1"
modulename="$2"
certname="$3"


# === Checks ===

[[ "$action" = "init" || "$action" = "update" ]] \
    || die "error: wrong action \"$action\""

# http://docs.puppetlabs.com/puppet/3/reference/modules_fundamentals.html#allowed-module-names
[[ "$modulename" =~ ^[a-z][a-z0-9_]*$ ]] \
    || die "error: invalid modulename \"$modulename\""

[[ -d "$module_dest_dir/$modulename" ]] \
    || die "error: there is no module \"$modulename\" in directory \"$module_dest_dir\""


# === Main ===

resources_dir="$PWD/resources"

tmpdir="$(mktemp -d)" \
    || die "Could not create temporary directory"
trap "cd; rm -fr \"${tmpdir}\"" EXIT

echo "info: cloning module repo"
cd "$tmpdir"
git clone --quiet "$module_dest_dir/$modulename" . \
    || die "error: could not clone git repository"
git config user.name "module client side script"
git config user.email "$USER@$(hostname)"
git config push.default current


if [[ "$action" = "init" ]]; then
    [[ ! -d .init ]] \
        || die "error: module already initialized"
    mkdir .init
else # update
    [[ -d .init ]] \
        || die "error: module not initialized"
    echo "class $modulename {" >manifests/init.pp
fi

echo "info: running resource scripts"
for resource in $resources_dir/*; do if [[ -x "$resource" ]]; then
    resource_name="$(basename $resource)"
    echo "info: resource $resource_name"
    
    if [[ "$action" = "init" ]]; then
        "$resource" init $certname </dev/null >.init/$resource_name
    else # update
        "$resource" update $certname <.init/$resource_name >>manifests/init.pp
    fi || die "error: $resource_name failed"
fi done

if [[ "$action" = "init" ]]; then
    git add .init
else # update
    echo "}" >>manifests/init.pp
fi


echo "info: pushing new configuration"
git commit --quiet -a -m "$action"
git push --quiet

echo "info: operation successfully finished"
