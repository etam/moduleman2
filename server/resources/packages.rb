#!/usr/bin/ruby

# packages[:provider]["name"] = "ensure"
def packages(node_certname)
    def package_fact?(name) name.to_s.start_with?("package ") end
    def strip_prefix(name) name["package ".length..-1] end
    
    def provider_fact?(name) name.end_with?(" provider") end
    def strip_suffix(name) name[0..-(" provider".length+1)] end
    
    require 'puppet'
    require 'puppet/face'
    Puppet.initialize_settings
    
    packages_facts = Puppet::Face[:facts, '0'].find(node_certname, {:terminus => :rest}).values \
        .delete_if { |name, value| not package_fact?(name) } \
        .inject({}) { |output, (name, value)| output[strip_prefix(name)] = value; output }
    
    providers_facts = packages_facts \
        .reject { |name, value| not provider_fact?(name) } \
        .inject({}) { |output, (name, value)| output[strip_suffix(name)] = value.to_sym; output }
    
    packages_facts.delete_if { |name, value| provider_fact?(name) }
    
    packages = Hash.new { |hash, key| hash[key] = {} }
    packages_facts.each_pair do |name, value|
        packages[providers_facts[name]][name] = value
    end
    
    packages
end

# ============================================

def print_manifest(package_info)
    puts "package { '#{package_info[:name]}':",
         "    ensure   => '#{package_info[:version]}',",
         "    provider => '#{package_info[:provider]}'",
         "}"
end

# ============================================

require 'yaml'

ignored_providers = [:gem, :windows]
ignored_states = ["absent"]

current_state =
    packages(ARGV[1]) \
        .delete_if { |provider, packages| ignored_providers.include?(provider) } \
        .each_pair { |provider, packages| packages.delete_if { |name, state| ignored_states.include?(state) } }

case ARGV[0]
when 'init'
    puts current_state.to_yaml
when 'update'
    orig_state = YAML::load(STDIN)
    current_state.each_pair do |provider, packages|
        packages.each_pair do |name, version|
            if not orig_state[provider].include?(name) or orig_state[provider][name] != version
                print_manifest({:provider=>provider, :name=>name, :version=>version})
            end
        end
    end
end
