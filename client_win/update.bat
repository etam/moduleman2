@echo off

cd %~dp0

call puppet_env.bat

PowerShell -NoProfile -ExecutionPolicy unrestricted -Command "& .\moduleman.ps1 update"
pause
