Param(
    [Parameter(Mandatory=$true, Position = 0)]
    [string]$action,
    [Parameter(Mandatory=$true)]
    [string]$modulename
)

. .\moduleman.conf.ps1

echo "info: running puppet agent"
puppet.bat agent --onetime --no-daemonize
echo "info: uploading facts"
puppet.bat facts upload

echo "info: calling server-side moduleman.sh $action"
$certname = puppet.bat config print certname
plink master@puppet "$moduleman_dir/moduleman.sh" $action $modulename $certname
