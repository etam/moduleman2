#!/bin/bash

die() {
    echo "$1" >&2
    exit 1
}

[[ $(id -u) == 0 ]] \
    || die "you must run this as root"

[[ $# -eq 2 ]] \
    || die "usage: $(basename $0) action modulename"

action="$1"
modulename="$2"

source moduleman.conf

echo "info: running puppet agent"
puppet agent --onetime --no-daemonize
echo "info: uploading facts"
puppet facts upload
echo "info: calling server-side moduleman.sh $action"
ssh master@puppet "$moduleman_dir/moduleman.sh" "$action" "$modulename" "$(puppet config print certname)"
